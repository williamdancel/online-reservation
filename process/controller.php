<?php 
	require_once('db_connection.php');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once ('../PHPMailer-master/src/Exception.php');
require_once ('../PHPMailer-master/src/PHPMailer.php');
require_once ('../PHPMailer-master/src/SMTP.php');

			

if(!empty($_POST["submit"]))
{

	if(!empty($_POST['mode']))
	{

		switch($_POST['mode'])
		{
			case ('registration'):

				$email = $_POST['email'];
				$lastname = $_POST['lastname'];
				$firstname = $_POST['firstname'];
				$middlename = $_POST['middlename'];
				$password = $_POST['password'];
				$confirm_password = $_POST['confirm_password'];
				$contactno = $_POST['contactno'];
				$address = $_POST['address'];
				$datenow= date('Y-m-d H:i:s');
				$password_encrpyted = password_hash($password, PASSWORD_DEFAULT);

			    $sql = "SELECT email FROM users WHERE email='$email'";
				$email_exists = $conn->query($sql);

				if($email_exists->num_rows > 0)
				{
					$email_exist = "Email Exists";
				}

				else if($password !== $confirm_password)
				{
					$password_not_match = "Password Not Match";
				} 
				else
				{
					$sql2 = "INSERT INTO users(firstname,middlename,lastname,email,password,contactno,address,role,status,created_date)
						VALUES ('$firstname', '$middlename', '$lastname','$email','$password_encrpyted','$contactno','$address','customer','active','$datenow')";
					

						$conn->query($sql2);

						session_start();
						$_SESSION["email"] = $email;
						header('Location:dashboard.php');

					
				}
				
				break;


			case('login'):


					$email = $_POST['email'];
					$password = $_POST['password'];

					$sql = "SELECT email FROM users WHERE email='$email'";
					$no_account = $conn->query($sql);


			   		 $sql2 = "SELECT password FROM users WHERE email='$email'";

			   		 $result = $conn->query($sql2);
					if ($result->num_rows > 0) {
					    // output data of each row
					    while($row = $result->fetch_assoc()) {
					       $password_hash = $row["password"];
					    }

					}
					if($no_account->num_rows == 0)
					{
						$no_account = "No Account";
					}


					else if(!password_verify($password, $password_hash))
					{
						$password_not_match = "Password didn't match";
					}
					else
					{
						session_start();
						$_SESSION["email"] = $email;
						header('Location:dashboard.php');
						
						
						
						
					}

					break;

			case('logout'):

					
					header('Location:login.php');
					session_destroy();
					break;

			case('editprofile'):
				$email = $_POST['email'];
				$lastname = $_POST['lastname'];
				$firstname = $_POST['firstname'];
				$middlename = $_POST['middlename'];
				$contactno = $_POST['contactno'];
				$address = $_POST['address'];


				if($lastname==null||$firstname==null||$middlename==null||$contactno==null||$address==null)
				{
					$emptyfield = "Empty Field";
				}
				else
				{
					$sql = "UPDATE users SET lastname='$lastname' , firstname='$firstname' , middlename='$middlename'
					, contactno = '$contactno' , address = '$address' WHERE email='$email'";

					$result=$conn->query($sql);

					if($result)
					{
						$success= "SUCCESS EDIT";
					}
				}
					
					
					break;

			case('editprofile_user'):
				$email = $_POST['email'];
				$lastname = $_POST['lastname'];
				$firstname = $_POST['firstname'];
				$middlename = $_POST['middlename'];
				$contactno = $_POST['contactno'];
				$address = $_POST['address'];
				$role = $_POST['role'];
				

				if($lastname==null||$firstname==null||$middlename==null||$contactno==null||$address==null)
				{
					$emptyfield = "Empty Field";
				}
				else
				{
					$sql = "UPDATE users SET lastname='$lastname' , firstname='$firstname' , middlename='$middlename'
					, contactno = '$contactno' , address = '$address' , role = '$role' WHERE email='$email'";

					$result=$conn->query($sql);

					if($result)
					{
						$success= "SUCCESS EDIT";
					}
				}
					
					
					break;

			case('editreservation'):

				$id = $_POST['id'];
				$email = $_POST['email'];
				$lastname = $_POST['lastname'];
				$contactno = $_POST['contactno'];
				$type_of_service = $_POST['type_of_service'];
				$category_of_service = $_POST['category_of_service'];
				$address = $_POST['address'];
				

				if($email==null||$lastname==null||$contactno==null||$type_of_service==null||$category_of_service==null||$address==null)
				{
					$emptyfield = "Empty Fields";
				}
				else
				{
					$sql = "UPDATE reservation SET lastname='$lastname' , email='$email' , type_of_service='$type_of_service'
					, contactno = '$contactno' , category_of_service = '$category_of_service' , address = '$address' WHERE id='$id'";

					$result=$conn->query($sql);

					if($result)
					{
						$success= "SUCCESS EDIT";
					}
				}
					
					
					break;
			case ('give_feedback'):

				$lastname = $_POST['lastname'];
				$email = $_POST['email'];
				$rate_of_service = $_POST['rate_of_service'];
				$message = $_POST['message'];
				$datenow= date('Y-m-d H:i:s');

				if($rate_of_service=='none')
				{
					$emptyrate = "Empty Rate";
				}
				else
				{


			  
					$sql2 = "INSERT INTO feedback(message_from,rate,message_from_email,message,created_at)
						VALUES ('$lastname','$rate_of_service','$email','$message','$datenow')";
					

						$conn->query($sql2);

						if($sql2)
						{
							$success = "Feedback Successfully Sent! Thank you";


						}
						else
						{
								$error = "Feedback Error Failed";
						}

					
				}
				
				break;
			case('editreservation_admin'):

				$id = $_POST['id'];
				$email = $_POST['email'];
				$therapist_email = $_POST['therapist_email'];
				$type_of_service = $_POST['type_of_service'];
				$category_of_service = $_POST['category_of_service'];
			
				

				if($email==null||$type_of_service==null||$category_of_service==null)
				{
					$emptyfield = "Empty Fields";
				}
				else
				{
					$sql = "UPDATE reservation SET therapist_email='$therapist_email' , email='$email' , type_of_service='$type_of_service' , category_of_service = '$category_of_service'  WHERE id='$id'";

					$result=$conn->query($sql);

					if($result)
					{
						$success= "SUCCESS EDIT";
					}
				}
					
					
					break;
				case('delete_feedback'):

					$id = $_POST['id'];


					$sql = "UPDATE feedback SET deleted_status = 'yes' WHERE id='$id'";

					$result=$conn->query($sql);

					if($result)
					{
						$error= "Deleted a Feedback!";
					}
				
					
					
					break;
			case('deactivate'):

					$id = $_POST['id'];


					$sql = "UPDATE users SET status = 'inactive' WHERE id='$id'";

					$result=$conn->query($sql);

					if($result)
					{
						$error= "Deactivated a Account!";
					}
				
					
					
					break;
			case('activate'):

					$id = $_POST['id'];

					
					$sql = "UPDATE users SET status = 'active' WHERE id='$id'";

					$result=$conn->query($sql);

					if($result)
					{
						$no_error= "Activated a Account!";
					}
				
					
					
					break;

			case('deactivate-reserv'):

					$id = $_POST['id'];


					$sql = "UPDATE reservation SET status_admin = 'inactive' WHERE id='$id'";

					$result=$conn->query($sql);

					if($result)
					{
						$error= "Deactivated a Reservation!";
					}
				
					
					
					break;
			case('cancel-reserv'):

					$id = $_POST['id'];


					$sql = "UPDATE reservation SET status_admin = 'inactive' WHERE id='$id'";

					$result=$conn->query($sql);

					if($result)
					{
						$error= "Cancel a Reservation!";
					}
				
					
					
					break;
			case('activate-reserv'):

					$id = $_POST['id'];


					
					$sql = "UPDATE reservation SET status_admin = 'active' WHERE id='$id'";

					$result=$conn->query($sql);

					if($result)
					{
						$no_error= "Activated a Reservation!";
					}
				
					
					
					break;
			case('accept-service'):

					$id = $_POST['id'];
					$therapist = $_POST['therapist'];
					$therapist_email = $_POST['therapist_email'];
					
					$sql = "UPDATE reservation SET status = 'on service',therapist = '$therapist',therapist_email = '$therapist_email' WHERE id='$id'";

					$result=$conn->query($sql);

					if($result)
					{
						$no_error= "Accepted a Reservation!";
					}
				
					
					
					break;
			case('done-service'):

					$id = $_POST['id'];
					$therapist = $_POST['therapist'];
					$therapist_email = $_POST['therapist_email'];
					
					$sql = "UPDATE reservation SET status = 'done service',therapist = '$therapist',therapist_email = '$therapist_email' WHERE id='$id'";

					$result=$conn->query($sql);

					if($result)
					{
						$no_error= "Done a Service Reservation!";
					}
				
					
					
					break;

				case ('request'):

				$email = $_POST['email'];



				$sqlsession = "SELECT * FROM users WHERE email='$email'";
				 $result = $conn->query($sqlsession);
					if ($result->num_rows > 0) {
					    // output data of each row
					    while($row = $result->fetch_assoc()) {
					       $lastname = $row["lastname"];
					       $contactno = $row["contactno"];
					       $address = $row['address'];
					    }

					}
				
				
				$type_of_service = $_POST['type_of_service'];
				$category_of_service = $_POST['category_of_service'];
				$date_time_sched  =  $_POST['datetime'];
				$datenow= date('Y-m-d H:i:s');


			    $sql = "SELECT date_time_sched FROM reservation WHERE status='approved' OR email='$email'";
				$datetime_lapse = $conn->query($sql);
					if ($datetime_lapse->num_rows > 0) {
						    // output data of each row
						    while($row = $datetime_lapse->fetch_assoc()) {
						       $date_time_sched_lapse = $row["date_time_sched"];
						    }

						}
					else
					{
						$date_time_sched_lapse = "";
					}


				if($type_of_service=="none")
				{
					$no_service = "Please select type of service!";
				}
				else if($category_of_service=="none")
				{
					$no_category = "Please select category of service!";
				}
				else if($date_time_sched==null)
				{
					$no_datetime = "No Date Time Selected!";
				}

				else
				{
					
					$sql2 = "INSERT INTO reservation(email,lastname,contactno,type_of_service,category_of_service,address,date_time_sched,status,therapist,therapist_email,status_admin,created_at)
						VALUES ('$email', '$lastname', '$contactno','$type_of_service','$category_of_service','$address','$date_time_sched','pending','pending therapist','pending therapist email','active','$datenow')";
					

						$results = $conn->query($sql2);


						 if($results)
							{
								$success= "SUCESSFULL REQUEST RESERVATION!";


								$sqlid = "SELECT * FROM reservation WHERE email='$email'";
								$result_id = $conn->query($sqlid);
								if ($result_id->num_rows > 0) {
								    // output data of each row
								    while($row = $result_id->fetch_assoc()) {
								       $id = $row["id"];
								       
								    }

								}

								// EMAILING SENDING

								$mail = new PHPMailer(); // create a new object
								$mail->IsSMTP(); // enable SMTP
								
								$mail->SMTPAuth = true; // authentication enabled
								$mail->SMTPSecure = 'tls://smtp.gmail.com'; // secure transfer enabled REQUIRED for Gmail
								$mail->Host = "smtp.gmail.com";
								$mail->Port = 587; // or 587
								$mail->IsHTML(true);
								$mail->SMTPOptions = array(
								'ssl' => array(
								'verify_peer' => false,
								'verify_peer_name' => false,
								'allow_self_signed' => true
								)
								);
								$mail->Username = "gilsanmrc@gmail.com";
								$mail->Password = "gilsanmrc_2018";
								$mail->SetFrom("gilsanmrc@gmail.com");
								$mail->Subject = "Verifying Reservation in Gilsan Massage and Reflexology Clinic";
								$mail->Body = "Good Day,<br>
												You have request a reservation for ". $type_of_service." and the date and time of your reservation is " . $date_time_sched ." and your chosen place is ".$category_of_service . " We are just verifying that you request this reservation.
												<a href='http://192.168.1.7/online-reservation/pages/verification_reservation.php?id=$id'>Click Here to Verify your reservation</a>";
								$mail->AddAddress($email);

								 if(!$mail->Send()) {
								    echo "Mailer Error: " . $mail->ErrorInfo;
								 } else {
								    echo "Message has been sent";


								 }

							}
						else
						{
							$error= "NOT RESERVED";
						}

						

					
				}
				
				break;



			}


		}

		

			
	}
		






 ?>