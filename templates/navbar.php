<?php 
$email = $_SESSION['email'];
 $sql = "SELECT * FROM users WHERE email='$email'";
 $result = $conn->query($sql);
          if ($result->num_rows > 0) {
              // output data of each row
              while($row = $result->fetch_assoc()) {
                 $role = $row["role"];
              }

          }
 ?>
 <!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <img src="../img/logo3.png" class="navbar-brand" style="width:85px">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="dashboard.php">
            <i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text">Dashboard</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Profile">
          <a class="nav-link" href="profile.php">
            <i class="fa fa-fw fa-user"></i>
            <span class="nav-link-text">Profile</span>
          </a>
        </li>
        <?php  if($role=='admin')
            {

        ?> 
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="User list">
          <a class="nav-link" href="userlist.php">
            <i class="fa fa-fw fa-table"></i>
            <span class="nav-link-text">User List</span>
          </a>
        </li>
        <?php } ?>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Reservation">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-calendar"></i>
            <span class="nav-link-text">Reservation</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseComponents">
            <?php 
            if($role!=='admin'&&$role!=='staff')
            {

             ?>
            <li>
              <a href="request_reservation.php">Request Reservation</a>
            </li>
            <?php } ?>
            <li>
              <a href="reservation_view.php">View Reservation List</a>
            </li>
          </ul>
        </li>
       
       <?php  if($role=='admin'||$role=='staff')
            {

        ?>  
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Feedback">
          <a class="nav-link" href="feedback.php">
            <i class="fa fa-fw fa-comment"></i>
            <span class="nav-link-text">Feedback</span>
          </a>
        </li>
        <?php } ?>
         <?php  if($role=='customer')
            {

        ?> 
          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Feedback">
          <a class="nav-link" href="give_feedback.php">
            <i class="fa fa-fw fa-comment"></i>
            <span class="nav-link-text">Give Feedback</span>
          </a>
        </li>
        <?php } ?>
         <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Homepage">
          <a class="nav-link" href="../index.php">
            <i class="fa fa-fw fa-home"></i>
            <span class="nav-link-text">Go to Homepage</span>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
        <h5 class="navbarsession">Welcome <?php 
          echo $_SESSION['email'];

         ?></h5>
       
       
        <li class="nav-item">
          <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-fw fa-sign-out"></i>Logout</a>
        </li>
      </ul>
    </div>
  </nav>