<?php include '../templates/header.php';
 require_once('../process/controller.php');

 if(session_id() == '') 
    {
      
        session_start();

        if(isset($_SESSION['email']) && !empty($_SESSION['email']))
        {
            header('Location: dashboard.php');
        }
    }

?>


<body class="bgimg-1">
  <div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header headercolor">Login</div>
      <div class="card-body">
            <img src="../img/logo3.png" class="logo2">
            <hr>

              <?php 
            if(!empty($no_account))
            {
            ?><h5 class="error"><?php  echo "Invalid Email Address or Password !";
            }
            else if(!empty($password_not_match))
            {
            ?><h5 class="error"><?php  echo "Invalid Email Address or Password!";
            }
            ?></h5>
        <form method="POST" action="">
           <input type = "hidden" name = "mode" value = "login">
          <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input class="form-control" id="exampleInputEmail1" type="email" aria-describedby="emailHelp" name="email" placeholder="Enter email" required>
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input class="form-control" id="exampleInputPassword1" name="password" type="password" placeholder="Password" required> 
          </div>
         
          <input class="btn btn-primary btn-block" name="submit" type="submit" value="Login"></input>
        </form>
        <div class="text-center">
          <a class="d-block small mt-3" href="register.php">Register an Account</a>
          <a class="d-block small" href="forgot-password.php">Forgot Password?</a>
          <a class="d-block small" href="../index.php">Go to Homepage?</a>
        </div>
      </div>
    </div>
  </div>


