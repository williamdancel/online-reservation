<?php 
include '../process/db_connection.php';
session_start();
$email = $_SESSION['email'];
 $sql = "SELECT * FROM users WHERE email='$email'";
 $result = $conn->query($sql);
          if ($result->num_rows > 0) {
              // output data of each row
              while($row = $result->fetch_assoc()) {
                 $role = $row["role"];
                 $session_name = $row['lastname'];
              }

          }
if(!isset($_SESSION["email"]))
{
 header('Location:login.php');
}



include '../templates/header-dashboard.php';
include '../process/controller.php';
?>

  <body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <?php include '../templates/navbar.php' ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="reservation_view">Reservation View</a>
        </li>

      </ol>
      <!-- ADMIN RESERVATION VIEW -->
     	<?php 
        	
		  if($role=='admin')
			{
				
        	$sql = "SELECT * FROM reservation ";
		    $result = $conn->query($sql);

		  $array= mysqli_fetch_all($result,MYSQLI_ASSOC);


		  

			

         ?>
		      <div class="container" style="background: lightblue; padding:10px;border-radius:10px">
		        <div class="card mb-3">
					        <div class="card-header">
					          <i class="fa fa-table"></i> Reservation Lists</div>
					        <div class="card-body">
					        	<?php 
						            if(!empty($error))
						            {
						            ?><h5 class="error"><?php  echo $error;
						            }
						            else if(!empty($no_error))
						            {
						            ?><h5 class="sucess"><?php  echo $no_error;
						            }
						            ?></h5>
					          <div class="table-responsive">
					            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					              <thead>
					                <tr>
					                  <th>ID</th>
					                  <th>Email</th>
					                  <th>Lastname</th>
					                  <th>Contact No</th>
					                  <th>Type of Service</th>
					                  <th>Date/Time Scheduled</th>
					                  <th>Category of Service</th>
					                  <th>Therapist</th>
					                  <th>Therapist Email</th>
					                  <th>Status</th>
					                  <th>Created Date Reservation</th>
					                  <th></th>
					                  <th></th>
					                
					                </tr>
					              </thead>
					           
					              <tbody>
					           
					               <?php 

					               foreach ($array as $row)
            						{ 
            							$email_user = $row['email'];
						                $sqlsession = "SELECT * FROM users WHERE email='$email_user'";
										 $result = $conn->query($sqlsession);
											if ($result->num_rows > 0) {
											    // output data of each row
											    while($rows = $result->fetch_assoc()) {
											       $id_user = $rows["id"];
											    }

											}
										
								   ?>
					                <tr>
					             
					                  <td><?php echo $row['id'] ?></td>
					                  <td><a href="profile_user_view.php?id=<?php echo $id_user ?>"><?php echo $row['email'] ?></a></td>
					                  <td><?php echo $row['lastname'] ?></td>
					                  <td><?php echo $row['contactno'] ?></td>
					                  <td><?php echo $row['type_of_service'] ?></td>
					                  <td><?php echo $row['date_time_sched'] ?></td>
					                  <td><?php echo $row['category_of_service'] ?></td>
					                  <td><?php echo $row['therapist'] ?></td>
					                  <td><?php echo $row['therapist_email'] ?></td>
					                  <td><?php echo $row['status'] ?></td>
					                  <td><?php echo $row['created_at'] ?></td>
					                  <td><a href="editreservation_admin.php?id=<?php echo $row['id']?>" class="btndel btn btn-info"><i class="fa fa-edit"></i>Assign Therapist</button></a>
					                  <?php 
					                  if($row['status_admin']=='active')
					                  {

					                   ?>
					                  <td>
					                  	<form action="" method="POST">
					                  		<input type="hidden" name="mode" value="deactivate-reserv"></input>
					                  		<input type="hidden" name="id" value="<?php echo $row['id']?>"></input>
					                  		<input type="submit" name="submit" style="cursor:pointer;" class=" btndel btn btn-danger" value="Deactivate"></input>
					                  	</form>
					              
					                  </td>
					                  <?php  
					              		} 
					              		else
					              		{


					              	   ?>
					              	   <td>
					                  	<form action="" method="POST">
					                  		<input type="hidden" name="mode" value="activate-reserv"></input>
					                  		<input type="hidden" name="id" value="<?php echo $row['id']?>"></input>
					                  		<input type="submit" name="submit" style="cursor:pointer;" class=" btndel btn btn-primary" value="Activate"></input>
					                  	</form>
					              
					                  </td>
					              	   <?php 
					              		} ?>
					              		 <?php 
					               		
					                   if($row['status']=='on service')
					                  {

					                   ?>
					                  <td>
					                  	<form action="" method="POST">
					                  		<input type="hidden" name="mode" value="done-service"></input>
					                  		<input type="hidden" name="id" value="<?php echo $row['id']?>"></input>
					                  		<input type="hidden" name="therapist" value="<?php echo $session_name ?>">
					                  		<input type="hidden" name="therapist_email" value="<?php echo $email ?>">
					                  		<input type="submit" name="submit" style="cursor:pointer;" class=" btndel btn btn-info" value="Done Service"></input>
					                  	</form>
					              
					                  </td>
					               	 
					                </tr>
					                 

					             <?php }} ?>

					              </tbody>
					            </table>
					          </div>
					        </div>
					        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
					      </div>

					      <!-- CUSTOMER RESERVATION VIEW -->
					      <?php } 
					       if($role=='customer')
							{
								$sql = "SELECT * FROM reservation WHERE email = '$email' ";
							    $result = $conn->query($sql);


							  	$array= mysqli_fetch_all($result,MYSQLI_ASSOC);

					      ?>

					        <div class="container" style="background: lightblue; padding:10px;border-radius:10px">
		        			<div class="card mb-3">
					        <div class="card-header">
					          <i class="fa fa-table"></i> Reservation Lists</div>
					        <div class="card-body">
					        	<?php 
						            if(!empty($error))
						            {
						            ?><h5 class="error"><?php  echo $error;
						            }
						            else if(!empty($no_error))
						            {
						            ?><h5 class="sucess"><?php  echo $no_error;
						            }
						            ?></h5>
					          <div class="table-responsive">
					            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					              <thead>
					                <tr>
					                  <th>ID</th>
					                  <th>Email</th>
					                  <th>Lastname</th>
					                  <th>Contact No</th>
					                  <th>Type of Service</th>
					                  <th>Date/Time Scheduled</th>
					                  <th>Category of Service</th>
					                  <th>Therapist</th>
					                  <th>Therapist Email</th>
					                  <th>Status</th>
					                  <th>Created Date Reservation</th>
					                  <th></th>
					                  <th></th>
					                
					                </tr>
					              </thead>
					           
					              <tbody>
					           
					               <?php foreach ($array as $row)
            						{
            							$email_user = $row['therapist_email'];
						                $sqlsession = "SELECT * FROM users WHERE email='$email_user'";
										 $result = $conn->query($sqlsession);
											if ($result->num_rows > 0) {
											    // output data of each row
											    while($rows = $result->fetch_assoc()) {
											       $id_user = $rows["id"];
											    }

											}

									if($row['status_admin']!=='inactive')
					                  {
								   ?>
					                <tr>
					             
					                  <td><?php echo $row['id'] ?></td>
					                  <td><?php echo $row['email'] ?></td>
					                  <td><?php echo $row['lastname'] ?></td>
					                  <td><?php echo $row['contactno'] ?></td>
					                  <td><?php echo $row['type_of_service'] ?></td>
					                  <td><?php echo $row['date_time_sched'] ?></td>
					                  <td><?php echo $row['category_of_service'] ?></td>
					                  <td><?php echo $row['therapist'] ?></td>
					                  <td><a href="profile_user_view.php?id=<?php echo $id_user ?>"><?php echo $row['therapist_email'] ?></a></td>
					                  <td><?php echo $row['status'] ?></td>
					                  <td><?php echo $row['created_at'] ?></td>
					                  <td><a href="editreservation.php?id=<?php echo $row['id']?>" class="btndel btn btn-info"><i class="fa fa-edit"></i> Edit</button></a>
					                
					                  <td>
					                  	<form action="" method="POST">
					                  		<input type="hidden" name="mode" value="cancel-reserv"></input>
					                  		<input type="hidden" name="id" value="<?php echo $row['id']?>"></input>
					                  		<input type="submit" name="submit" style="cursor:pointer;" class=" btndel btn btn-danger" value="Cancel Reservation"></input>
					                  	</form>
					              
					                  </td>
					                 
					               	 
					                </tr>
					             <?php } } ?>
					              </tbody>
					            </table>
					          </div>
					        </div>
					        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
					      </div>
					      <!-- STAFF RESERVATION VIEW -->
					      <?php }

					      if($role=='staff')
							{
								$sql = "SELECT * FROM reservation";
							    $result = $conn->query($sql);

							  	$array= mysqli_fetch_all($result,MYSQLI_ASSOC);
							  
					       ?>
					         <div class="container" style="background: lightblue; padding:10px;border-radius:10px">
		        			<div class="card mb-3">
					        <div class="card-header">
					          <i class="fa fa-table"></i> Reservation Lists</div>
					        <div class="card-body">
					        	<?php 
						            if(!empty($error))
						            {
						            ?><h5 class="error"><?php  echo $error;
						            }
						            else if(!empty($no_error))
						            {
						            ?><h5 class="sucess"><?php  echo $no_error;
						            }
						            ?></h5>
					          <div class="table-responsive">
					            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					              <thead>
					                <tr>
					                  <th>ID</th>
					                  <th>Email</th>
					                  <th>Lastname</th>
					                  <th>Contact No</th>
					                  <th>Type of Service</th>
					                  <th>Date/Time Scheduled</th>
					                  <th>Category of Service</th>
					                  <th>Therapist</th>
					                  <th>Therapist Email</th>
					                  <th>Status</th>
					                  <th>Created Date Reservation</th>
					                  <th></th>
					                
					                </tr>
					              </thead>
					           
					              <tbody>
					           
					               <?php foreach ($array as $row)
            						{
            							$email_user = $row['email'];
						                $sqlsession = "SELECT * FROM users WHERE email='$email_user'";
										 $result = $conn->query($sqlsession);
											if ($result->num_rows > 0) {
											    // output data of each row
											    while($rows = $result->fetch_assoc()) {
											       $id_user = $rows["id"];
											    }

											}
								   ?>
					             
					                  <td><?php echo $row['id'] ?></td>
					                  <td><a href="profile_user_view.php?id=<?php echo $id_user ?>"><?php echo $row['email'] ?></a></td>
					                  <td><?php echo $row['lastname'] ?></td>
					                  <td><?php echo $row['contactno'] ?></td>
					                  <td><?php echo $row['type_of_service'] ?></td>
					                  <td><?php echo $row['date_time_sched'] ?></td>
					                  <td><?php echo $row['category_of_service'] ?></td>
					                  <td><?php echo $row['therapist'] ?></td>
					                  <td><?php echo $row['therapist_email'] ?></td>
					                  <td><?php echo $row['status'] ?></td>
					                  <td><?php echo $row['created_at'] ?></td>
					                  
					                  <?php 
					                  if($row['status']!=='on service' && $row['status']!=='pending')
					                  {

					                   ?>
					                  <td>
					                  	<form action="" method="POST">
					                  		<input type="hidden" name="mode" value="accept-service"></input>
					                  		<input type="hidden" name="id" value="<?php echo $row['id']?>"></input>
					                  		<input type="hidden" name="therapist" value="<?php echo $session_name ?>">
					                  		<input type="hidden" name="therapist_email" value="<?php echo $email ?>">
					                  		<input type="submit" name="submit" style="cursor:pointer;" class=" btndel btn btn-info" value="Accept Service"></input>
					                  	</form>
					              
					                  </td>
					                   <?php 
					               		}
					                  else if($row['status']=='on service')
					                  {

					                   ?>
					                  <td>
					                  	<form action="" method="POST">
					                  		<input type="hidden" name="mode" value="done-service"></input>
					                  		<input type="hidden" name="id" value="<?php echo $row['id']?>"></input>
					                  		<input type="hidden" name="therapist" value="<?php echo $session_name ?>">
					                  		<input type="hidden" name="therapist_email" value="<?php echo $email ?>">
					                  		<input type="submit" name="submit" style="cursor:pointer;" class=" btndel btn btn-info" value="Done Service"></input>
					                  	</form>
					              
					                  </td>
					                  <?php  
					                  	}
					              		
					              		else
					              		{


					              		?>
					              			<td></td>
					              		<?php } ?>
					                </tr>
					         		  <?php 
					              		} ?>
					              </tbody>
					            </table>
					          </div>
					        </div>
					        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
					            <?php } ?>
					      </div>
					</div>
					<hr>
				</div>
		      </div>
		   
		    
        </div>
       </div>

  <?php include '../templates/footer-dashboard.php' ?>