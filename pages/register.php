<?php include '../templates/header.php';
      require_once('../process/controller.php');
 ?>

<body class="bgimg-1">
  <div class="container">
    <div class="card card-register mx-auto mt-5">
      <div class="card-header headercolor">Register an Account</div>
      <div class="card-body">
         <img src="../img/logo3.png" class="logo2">
         <hr>
         <?php 
            if(!empty($email_exist))
            {
            ?><h5 class="error"><?php  echo "Email Already Exists!";
            }
            else if(!empty($password_not_match))
            {
            ?><h5 class="error"><?php  echo "Password Not Match!";
            }
            ?></h5>


        <form action="" method="POST">
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-4">
                <input type = "hidden" name = "mode" value = "registration">
                <label for="exampleInputName">First name</label>
                <input class="form-control" name="firstname" id="exampleInputName" type="text" aria-describedby="nameHelp" placeholder="Enter first name" required>
              </div>
              <div class="col-md-4">
                <label for="exampleInputMiddleName">Middle name</label>
                <input class="form-control" name="middlename" id="exampleInputMiddleName" type="text" aria-describedby="nameHelp" placeholder="Enter middle name" required>
              </div>
               <div class="col-md-4">
                <label for="exampleInputLastName">Last name</label>
                <input class="form-control" name="lastname" id="exampleInputLastName" type="text" aria-describedby="nameHelp" placeholder="Enter last name" required>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input class="form-control" name="email" id="exampleInputEmail1" type="email" aria-describedby="emailHelp" placeholder="Enter email" required>
          </div>
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label for="exampleInputPassword1">Password</label>
                <input class="form-control" name="password" id="exampleInputPassword1" type="password" placeholder="Password" required>
              </div>
              <div class="col-md-6">
                <label for="exampleConfirmPassword">Confirm password</label>
                <input class="form-control" name="confirm_password" id="exampleConfirmPassword" type="password" placeholder="Confirm password" required>
              </div>
            </div>
          </div>
          <input class="btn btn-primary btn-block" name="submit" type="submit" value="Register"></input>
        </form>
        <div class="text-center">
          <a class="d-block small mt-3" href="login.php">Login Page</a>
          <a class="d-block small" href="forgot-password.php">Forgot Password?</a>
        </div>
      </div>
    </div>
  </div>
\
