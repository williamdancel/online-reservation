<?php 
include '../process/db_connection.php';
session_start();

if(!isset($_SESSION["email"]))
{
 header('Location:login.php');
}


include '../templates/header-dashboard.php';
include '../process/controller.php';
?>

  <body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <?php include '../templates/navbar.php' ?>
  <div class="content-wrapper">
    <div class="container-fluid">
    	 <a href="reservation_view.php" class="btn btn-success">Back</a>
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="profile.php">Profile</a>
        </li>

      </ol>
     	<?php
     			$id = $_GET['id']; 
        	
        	$sql = "SELECT * FROM users WHERE id='$id'";
		    $result = $conn->query($sql);

		    if ($result->num_rows > 0) {
			// output data of each row
			

			 while($row = $result->fetch_assoc()) {
			 	   $emailuser = $row['email'];
				   $firstname = $row["firstname"];
				   $lastname = $row["lastname"];
				   $middlename = $row["middlename"];
				   $contactno = $row['contactno'];
				   $address = $row['address'];
				   $role = $row['role'];
			}
		   }

         ?>
		      <div class="container" style="background: lightblue; padding:10px;border-radius:10px">
		         <h5 class="fontheader">Email: <b><?php echo $emailuser ?></b></h5>
		         <h5 class="fontheader">Lastname: <b><?php echo $lastname ?></b></h5>
		         <h5 class="fontheader">Firstname: <b><?php echo $firstname ?></b></h5>
		         <h5 class="fontheader">Middlename: <b><?php echo $middlename ?></b></h5>
		         <h5 class="fontheader">Contact No.: <b><?php echo $contactno ?></b></h5>
		         <h5 class="fontheader">Address: <b><?php echo $address ?></b></h5>
		         <h5 class="fontheader">Role: <b><?php echo $role ?></b></h5>
		      </div>
		      <br>
		      
        </div>
       </div>

  <?php include '../templates/footer-dashboard.php' ?>