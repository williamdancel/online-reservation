<?php 
include '../process/db_connection.php';
require_once('../process/controller.php');

session_start();

if(!isset($_SESSION["email"]))
{
 header('Location:login.php');
}


include '../templates/header-dashboard.php';



?>

  <body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <?php include '../templates/navbar.php' ?>
  <div class="content-wrapper">
    <div class="container-fluid">
    	 <a href="dashboard.php" class="btn btn-success">Back</a>
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Give Feedback</a>
        </li>

      </ol>
      <?php $sql = "SELECT * FROM users WHERE email='$email'";
		 		   $result = $conn->query($sql);

		   

		    if ($result->num_rows > 0) {
			// output data of each row
			

			 while($row = $result->fetch_assoc()) {
			 	   $lastname = $row["lastname"];
				  
					}
		     	 } ?>
     	
		      <div class="container" style="background: lightblue; padding:10px;border-radius:10px">
    			<?php 
    				
		            if(!empty($emptyrate))
		            {
		            ?><h5 class="error"><?php  echo "Please Rate Us";
		            }
		            else if(!empty($success))
		            {
		            ?><h5 class="sucess"><?php  echo $success;
		            }	
		            
		            ?></h5>
		         <form method="POST" action="">
		         	<input type="hidden" name="mode" value="give_feedback"></input>

		         	<div class="form-group">
		         		<label for="email">Email: </label>
		         		<input class="form-control" type="text" name="email" value=<?php echo $_SESSION['email'] ?> maxlength="100" readonly=""></input>
		         	</div>
		         	<div class="form-group">
		         		<label for="lastname">Lastname: </label>
		         		<input class="form-control" type="text" name="lastname" value=<?php echo $lastname ?> maxlength="50" readonly=""></input>
		         	</div>

		         	<div class="form-group">
		         		<label for="message">Message: </label>
		         		<input class="form-control" type="text" name="message" maxlength="100" ></input>
		         	</div>

		         	<div class="form-group">
		         		<label for="role">Rate of Service: </label>
		         		<select class="form-control" type="text" name="rate_of_service" >
		         			  <option value="none">Select Rate</option>
				              <option >5 Star</option>
				              <option >4 Star</option>
				              <option >3 Star</option>
				              <option >2 Star</option>
				              <option >1 Star</option>

		         		</select>
		         	</div>

		         	<input class="btn btn-primary " name="submit" type="submit" style="cursor:pointer" value="Save"></input>
			         </form>
			      </div>
			  	</div>
			      <br>
		     
        </div>
       </div>

  <?php include '../templates/footer-dashboard.php' ?>