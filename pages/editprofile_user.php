<?php 
include '../process/db_connection.php';
require_once('../process/controller.php');

session_start();

if(!isset($_SESSION["email"]))
{
 header('Location:login.php');
}


include '../templates/header-dashboard.php';
include '../process/controller.php';
?>

  <body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <?php include '../templates/navbar.php' ?>
  <div class="content-wrapper">
    <div class="container-fluid">
    	 <a href="userlist.php" class="btn btn-success">Back</a>
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Edit Profile</a>
        </li>

      </ol>
     	<?php 
     	 $id = $_GET['id'];
     	 
     	 if(!empty($id))
     	 {

        	$sql = "SELECT * FROM users WHERE id='$id'";
		    $result = $conn->query($sql);

		   

		    if ($result->num_rows > 0) {
			// output data of each row
			

			 while($row = $result->fetch_assoc()) {
			 	   $email_user = $row["email"];
				   $firstname = $row["firstname"];
				   $lastname = $row["lastname"];
				   $middlename = $row["middlename"];
				   $contactno = $row['contactno'];
				   $address = $row['address'];
				   $role = $row['role'];
					}
		     	 }
     	 }
     	 

         ?>
		      <div class="container" style="background: lightblue; padding:10px;border-radius:10px">
    			<?php 
		            if(!empty($emptyfield))
		            {
		            ?><h5 class="error"><?php  echo "Please Input All Fields!";
		            }
		            else if(!empty($success))
		            {
		            ?><h5 class="sucess"><?php  echo "Edit Successful!";
		            }	
		            
		            ?></h5>
		         <form method="POST" action="">
		         	<input type="hidden" name="mode" value="editprofile_user"></input>

		         	<div class="form-group">
		         		<label for="email">Email: </label>
		         		<input class="form-control" type="text" name="email" value=<?php echo $email_user?> maxlength="100"></input>
		         	</div>
		         	<div class="form-group">
		         		<label for="lastname">Lastname: </label>
		         		<input class="form-control" type="text" name="lastname" maxlength="50" value=<?php echo $lastname?>></input>
		         	</div>
		         	<div class="form-group">
		         		<label for="Firstname">Firstname: </label>
		         		<input class="form-control" type="text" name="firstname" maxlength="50" value=<?php echo $firstname?>></input>
		         	</div>

		         	<div class="form-group">
		         		<label for="middlename">Middlename: </label>
		         		<input class="form-control" type="text" name="middlename" maxlength="30" value=<?php echo $middlename?>></input>
		         	</div>

		         	<div class="form-group">
		         		<label for="contactno">Contact No: </label>
		         		<input class="form-control" type="text" name="contactno" maxlength="100" value=<?php echo $contactno?>></input>
		         	</div>

		         	<div class="form-group">
		         		<label for="address">Address: </label>
		         		<input class="form-control" type="text" name="address" maxlength="500" value=<?php echo $address?>></input>
		         	</div>

		         	<div class="form-group">
		         		<label for="role">Role: </label>
		         		<select class="form-control" type="text" name="role" >
		         			<option value="<?php echo $role?>"><?php echo $role?> - Selected</option>
		         			<option value="admin">Admin</option>
		         			<option value="staff">Staff</option>
		         			<option value="customer">Customer</option>

		         		</select>
		         	</div>

		         	<input class="btn btn-primary " name="submit" type="submit" style="cursor:pointer" value="Save"></input>
			         </form>
			      </div>
			  	</div>
			      <br>
		     
        </div>
       </div>

  <?php include '../templates/footer-dashboard.php' ?>