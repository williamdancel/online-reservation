<?php 
include '../process/db_connection.php';
require_once('../process/controller.php');

session_start();

if(!isset($_SESSION["email"]))
{
 header('Location:login.php');
}


include '../templates/header-dashboard.php';
include '../process/controller.php';
?>

  <body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <?php include '../templates/navbar.php' ?>
  <div class="content-wrapper">
    <div class="container-fluid">
    	 <a href="reservation_view.php" class="btn btn-success">Back</a>
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Edit Reservation</a>
        </li>

      </ol>
     	<?php 
     	 $id = $_GET['id'];
     	 
     	 if(!empty($id))
     	 {

        	$sql = "SELECT * FROM reservation WHERE id='$id'";
		    $result = $conn->query($sql);
		     $id = $_GET['id'];
		   

		    if ($result->num_rows > 0) {
			// output data of each row
			

			 while($row = $result->fetch_assoc()) {
			 	   $email_user = $row["email"];
				   $lastname = $row["lastname"];
				   $type_of_service = $row["type_of_service"];
				   $category_of_service = $row["category_of_service"];
				   $contactno = $row['contactno'];
				   $therapist = $row['therapist'];
				   $address = $row['address'];
				 
					}
		     	 }
     	 }
     	 

         ?>
		      <div class="container" style="background: lightblue; padding:10px;border-radius:10px">
    			<?php 
		            if(!empty($emptyfield))
		            {
		            ?><h5 class="error"><?php  echo "Please Input All Fields!";
		            }
		            else if(!empty($success))
		            {
		            ?><h5 class="sucess"><?php  echo "Edit Successful!";
		            }	
		            
		            ?></h5>
		         <form method="POST" action="">
		         	<input type="hidden" name="mode" value="editreservation"></input>

		         	<div class="form-group">
		         		<label for="email">Email: </label>
		         		<input class="form-control" type="text" name="email" value=<?php echo $email_user?> maxlength="100" readonly="readonly"></input>
		         	</div>

		         	<input type="hidden" name="id" value="<?php echo $id ?>">
		         	<div class="form-group">
		         		<label for="lastname">Lastname: </label>
		         		<input class="form-control" type="text" name="lastname" maxlength="50" value=<?php echo $lastname?> readonly="readonly"></input>
		         	</div>
		         	<div class="form-group">
		         		<label for="address">Address: </label>
		         		<input class="form-control" type="text" name="address" maxlength="250" value=<?php echo $address?>></input>
		         	</div>
		         	<div class="form-group">
		         		<label for="type_of_service">Type of Service:</label>
		         		<select class="form-control" type="text" name="type_of_service" >
		         			<option value="<?php echo $type_of_service?>"><?php echo $type_of_service?> - Selected</option>
		         			<option>Reflexology</option>
		         			<option>Massage</option>

		         		</select>
		         	</div>
		         	<div class="form-group">
		         		<label for="type_of_service">Category of Service:</label>
		         		<select class="form-control" type="text" name="category_of_service" >
		         			<option value="<?php echo $category_of_service?>"><?php echo $category_of_service?> - Selected</option>
		         			<option>In Home</option>
		         			<option>In Clinic</option>

		         		</select>
		         	</div>
		         	
		         	<div class="form-group">
		         		<label for="contactno">Contact No: </label>
		         		<input class="form-control" type="text" name="contactno" maxlength="100" value=<?php echo $contactno?>></input>
		         	</div>

		         	

		         	<input class="btn btn-primary " name="submit" type="submit" style="cursor:pointer" value="Save"></input>
			         </form>
			      </div>
			  	</div>
			      <br>
		     
        </div>
       </div>

  <?php include '../templates/footer-dashboard.php' ?>