<?php 
session_start();
include '../templates/header-dashboard.php';
if(!isset($_SESSION["email"]))
{
 header('Location:login.php');
}


include '../process/controller.php';
?>

 <?php $sql = "SELECT * FROM feedback";
           $result = mysqli_query($conn,$sql);

       $count_feedback = $result->num_rows;
      
          $sql2 = "SELECT * FROM users WHERE role='customer'";
           $result2 = mysqli_query($conn,$sql2);

       $count_customer = $result2->num_rows;

       $sql3 = "SELECT * FROM reservation";
           $result3 = mysqli_query($conn,$sql3);

       $count_reservation = $result3->num_rows;

       $sql4 = "SELECT * FROM users WHERE role='staff'";
           $result4 = mysqli_query($conn,$sql4);

       $count_staff = $result4->num_rows;
      

            
            ?>
      
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <?php include '../templates/navbar.php' ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="dashboard.php">Dashboard</a>
        </li>
      
      </ol>
      <div class="row">
        <div class="col-12">
<!-- Icon Cards-->
      <div class="row">
        <div class="col-xl-3 col-sm-6 mb-3">
          <div class="card text-white bg-primary o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-fw fa-comments"></i>
              </div>
              <div class="mr-5"><?php echo $count_feedback ?> List Feedbacks!</div>
            </div>
           
          </div>
        </div>
        <div class="col-xl-3 col-sm-6 mb-3">
          <div class="card text-white bg-warning o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-fw fa-list"></i>
              </div>
              <div class="mr-5"><?php echo $count_reservation ?> List of Reservation!</div>
            </div>
            
          </div>
        </div>
        <div class="col-xl-3 col-sm-6 mb-3">
          <div class="card text-white bg-success o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-fw fa-users"></i>
              </div>
              <div class="mr-5"><?php echo $count_customer ?> List of Customer!</div>
            </div>
           
          </div>
        </div>
        <div class="col-xl-3 col-sm-6 mb-3">
          <div class="card text-white bg-danger o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-fw fa-support"></i>
              </div>
              <div class="mr-5"><?php echo $count_staff ?> List of all Staffs</div>
            </div>
        
          </div>
        </div>
      </div>
        </div>
      </div>
    </div>
  
      
 
       
  <?php include '../templates/footer-dashboard.php' ?>
 