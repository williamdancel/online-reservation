<?php 
include '../process/controller.php';
include '../process/db_connection.php';
require_once('../process/controller.php');

session_start();

if(!isset($_SESSION["email"]))
{
 header('Location:login.php');
}


include '../templates/header-dashboard.php';

?>

  <body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <?php include '../templates/navbar.php' ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="request_reservation.php">Request a Reservation</a>
        </li>

      </ol>

		      <div class="container" style="background: lightblue; padding:10px;border-radius:10px">
    			<?php 
    				if(!empty($success))
		            {

		            ?><h5 class="sucess"><?php  echo $success;
		            }	
		            else if(!empty($datetimelapse))
		            {
		            ?><h5 class="error"><?php  echo $datetimelapse;
		            }
		            else if(!empty($no_service))
		            {
		            ?><h5 class="error"><?php  echo $no_service;
		            }	

		            else if(!empty($no_category))
		            {
		            ?><h5 class="error"><?php  echo $no_category;
		            }	
		             else if(!empty($no_datetime))
		            {
		            ?><h5 class="error"><?php  echo $no_datetime;
		            }	
		              else if(!empty($error))
		            {
		            ?><h5 class="error"><?php  echo $error;
		            }	

        			$email = $_SESSION['email'];
		            
		            ?></h5>
		         <form method="POST" action="">
		         	<input type="hidden" name="mode" value="request"></input>
		         	<div class="form-group">
					    <label for="dtp_input1" class="col-md-2 control-label">DateTime Reservation</label>
					                <div class="input-group date form_datetime col-md-5" data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input1">
					                    <input class="form-control" size="16" type="text" value="" name="datetime" readonly>
					                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
					          <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
					                </div>
					        <input type="hidden" id="dtp_input1" value="" /><br/>
					  </div>

		         	<div class="form-group">
		         		<label for="email">Email: </label>
		         		<input class="form-control" type="text" name="email" value=<?php echo $email?> maxlength="100" readonly="readonly"></input>
		         	</div>
		         	<div class="form-group">
		         		<label for="type_of_service">Type of Service: </label>
		         		<select class="form-control" type="text" name="type_of_service" >
		         			<option value="none">Select Service</option>
		         			<option >Massage</option>
		         			<option >Reflexology</option>
		         		</select>
		         	</div>
		         	<div class="form-group">
		         		<label for="category_of_service">Category of Service: </label>
		         		<select class="form-control" type="text" name="category_of_service" >
		         			<option value="none">Select Category Service</option>
		         			<option>At Home</option>
		         			<option>At Clinic</option>
		         		</select>
		         	</div>
		         	
		         	<input class="btn btn-primary " name="submit" type="submit" style="cursor:pointer" value="Send"></input>
			         </form>
			      </div>
			  	</div>
			      <br>
		     
        </div>
       </div>

  <?php include '../templates/footer-dashboard.php' ?>