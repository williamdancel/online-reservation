<?php include '../templates/header.php' ?>

<body class="bgimg-1">
  <div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header headercolor">Reset Password</div>
      <div class="card-body">
        <img src="../img/logo3.png" class="logo2">
        <hr>
        <div class="text-center mt-4 mb-5">
          <h4>Forgot your password?</h4>
          <p>Enter your email address and we will send you instructions on how to reset your password.</p>
        </div>
        <form>
          <div class="form-group">
            <input class="form-control" id="exampleInputEmail1" type="email" aria-describedby="emailHelp" placeholder="Enter email address">
          </div>
          <button class="btn btn-primary btn-block" type="submit">Submit</button>
        </form>
        <div class="text-center">
          <a class="d-block small mt-3" href="register.php">Register an Account</a>
          <a class="d-block small" href="login.php">Login Page</a>
        </div>
      </div>
    </div>
  </div>

