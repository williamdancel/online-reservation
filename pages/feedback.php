
<?php 
include '../process/db_connection.php';
session_start();
$email = $_SESSION['email'];
 $sql = "SELECT * FROM users WHERE email='$email'";
 $result = $conn->query($sql);
          if ($result->num_rows > 0) {
              // output data of each row
              while($row = $result->fetch_assoc()) {
                 $role = $row["role"];
              }

          }
if(!isset($_SESSION["email"]))
{
 header('Location:login.php');
}
else if($role!=='admin')
{
 header('Location:dashboard.php');
}


include '../templates/header-dashboard.php';
include '../process/controller.php';
?>

  <body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <?php include '../templates/navbar.php' ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Feedback List</a>
        </li>

      </ol>
     	<?php 
        		
        	$sql = "SELECT * FROM feedback ";
		    $result = $conn->query($sql);

		  $array= mysqli_fetch_all($result,MYSQLI_ASSOC);
		   

         ?>
		      <div class="container" style="background: lightblue; padding:10px;border-radius:10px">
		        <div class="card mb-3">
					        <div class="card-header">
					          <i class="fa fa-table"></i> User Lists</div>
					        <div class="card-body">
					        	<?php 
						            if(!empty($error))
						            {
						            ?><h5 class="error"><?php  echo $error;
						            }
						            else if(!empty($no_error))
						            {
						            ?><h5 class="sucess"><?php  echo $no_error;
						            }
						            ?></h5>
					          <div class="table-responsive">
					            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					              <thead>
					                <tr>
					                  <th>ID</th>
					                  <th>Rate of Service</th>
					                  <th>Message From</th>
					                  <th>Email</th>
					                  <th>Registered Date</th>
					                  <th></th>
					                
					                </tr>
					              </thead>
					           
					              <tbody>
					           
					               <?php foreach ($array as $row)
            						{
								   ?>
					                <tr>
					             	  <?php if($row['deleted_status']!=='yes')
					             	  {
					             	   ?>
					                  <td><?php echo $row['id'] ?></td>
					                  <td><?php echo $row['rate'] ?></td>
					                  <td><?php echo $row['message_from'] ?></td>
					                  <td><?php echo $row['message_from_email'] ?></td>
					                  <td><?php echo $row['created_at'] ?></td>
					                
					               
					                  <td>
					                  	<form action="" method="POST">
					                  		<input type="hidden" name="mode" value="delete_feedback"></input>
					                  		<input type="hidden" name="id" value="<?php echo $row['id']?>"></input>
					                  		<input type="submit" name="submit" style="cursor:pointer;" class=" btndel btn btn-danger" value="Delete"></input>
					                  	</form>
					              
					                  </td>
					                
					              	  
					              	 
					                </tr>
					                <?php } ?>
					             <?php } ?>
					              </tbody>
					            </table>
					          </div>
					        </div>
					        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
					      </div>

					</div>
					<hr>
				</div>
		      </div>
		      <br>
		      <a class="btn btn-info" href="editprofile.php"><i class="fa fa-edit"></i>Edit Profile</a>
        </div>
       </div>

  <?php include '../templates/footer-dashboard.php' ?>