<?php 
include 'process/db_connection.php';
include 'process/controller_guess.php';
             session_start();

             if(isset($_SESSION["email"]))
             {
                  $email = $_SESSION["email"];
             }
           
?>
<!DOCTYPE html>
<html>
<title>Gilsan Massage and Reflexology Clinic</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSS -->
  <link rel="stylesheet" href="css/w3.css">
  <!-- Bootstrap core CSS -->
       <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="css/sb-admin.css" rel="stylesheet" type="text/css">
        <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon/favicon.ico" />
        <script src='https://www.google.com/recaptcha/api.js'></script> 
         <link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
        <!-- FAVICON -->

      <link rel="apple-touch-icon" sizes="57x57" href="img/apple-icon-57x57.png">
      <link rel="apple-touch-icon" sizes="60x60" href="img/apple-icon-60x60.png">
      <link rel="apple-touch-icon" sizes="72x72" href="img/apple-icon-72x72.png">
      <link rel="apple-touch-icon" sizes="76x76" href="img/apple-icon-76x76.png">
      <link rel="apple-touch-icon" sizes="114x114" href="img/apple-icon-114x114.png">
      <link rel="apple-touch-icon" sizes="120x120" href="img/apple-icon-120x120.png">
      <link rel="apple-touch-icon" sizes="144x144" href="img/apple-icon-144x144.png">
      <link rel="apple-touch-icon" sizes="152x152" href="img/apple-icon-152x152.png">
      <link rel="apple-touch-icon" sizes="180x180" href="img/apple-icon-180x180.png">
      <link rel="icon" type="image/png" sizes="192x192"  href="img/android-icon-192x192.png">
      <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
      <link rel="icon" type="image/png" sizes="96x96" href="img/favicon-96x96.png">
      <link rel="icon" type="image/png" sizes="16x16" href="img/favicon-16x16.png">
      <link rel="manifest" href="/manifest.json">
      <meta name="msapplication-TileColor" content="#ffffff">
      <meta name="msapplication-TileImage" content="img/ms-icon-144x144.png">
      <meta name="theme-color" content="#ffffff">

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/popper/popper.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>

    <script type="text/javascript">
      // Modal Image Gallery
      function onClick(element) {
        document.getElementById("img01").src = element.src;
        document.getElementById("modal01").style.display = "block";
        var captionText = document.getElementById("caption");
        captionText.innerHTML = element.alt;
      }
    </script>

    <!-- Custom scripts for this template -->
    <script src="js/sb-admin.js"></script>
    <script src="js/homepage.js"></script>


  <!-- internal css -->
<style>
body,h1,h2,h3,h4,h5,h6 {font-family: "Calibri", sans-serif;}
body, html {
    height: 100%;
    color: #777;
    line-height: 1.8;
}

</style>
<body>

<!-- Navbar (sit on top) -->
<div class="w3-top">
  <div class="w3-bar" id="myNavbar">
    <a class="w3-bar-item w3-button w3-hover-black w3-hide-medium w3-hide-large w3-right" href="javascript:void(0);" onclick="toggleFunction()" title="Toggle Navigation Menu">
      <i class="fa fa-bars"></i>
    </a>
    <a href="#home" class="w3-bar-item w3-button"><i class="fa fa-home"></i> HOME</a>
    <a href="#about" class="w3-bar-item w3-button w3-hide-small"><i class="fa fa-user"></i> ABOUT</a>
    <a href="#staffs" class="w3-bar-item w3-button w3-hide-small"><i class="fa fa-users"></i> STAFFS</a>
    <a href="#contactus" class="w3-bar-item w3-button w3-hide-small"><i class="fa fa-phone"></i> CONTACT US</a>
    <img src="img/logo3.png" class="logo">
      <?php  if(!isset($_SESSION["email"]))
             {
              
             
            ?>
    <a href="pages/register.php" class="w3-bar-item w3-button w3-hide-small" style="float:right"><i class="fa fa-user-plus"></i> REGISTER</a>
    <a href="pages/login.php" class="w3-bar-item w3-button w3-hide-small" style="float:right"><i class="fa fa-sign-in"></i> LOGIN</a>
    <?php }
    else
    {

     ?>
     <h5  style="float:right;margin-top:10px">You Are Login as <?php echo $email ?></h5>
      <a href="pages/login.php" class="w3-bar-item w3-button w3-hide-small" style="float:right"><i class="fa fa-sign-in"></i> Dashboard</a>
    <?php } ?>
    </a>
  </div>

  <!-- Navbar on small screens -->
  <div id="navDemo" class="w3-bar-block w3-white w3-hide w3-hide-large w3-hide-medium">
    <a href="#about" class="w3-bar-item w3-button" onclick="toggleFunction()">ABOUT</a>
    <a href="#staffs" class="w3-bar-item w3-button" onclick="toggleFunction()">STAFFS</a>
    <a href="#contactus" class="w3-bar-item w3-button" onclick="toggleFunction()">CONTACT US</a>


   <?php  if(isset($_SESSION["email"]))
             {
                  $email = $_SESSION["email"];
             
            ?>
    <a href="pages/login.php" class="w3-bar-item w3-button" onclick="toggleFunction()">LOGIN</a>
    <a href="pages/register.php" class="w3-bar-item w3-button" onclick="toggleFunction()">REGISTER</a>
    <?php } ?>
  </div>
</div>

<!-- First Parallax Image with Logo Text -->
<div class="bgimg-1 w3-display-container w3-opacity-min" id="home">
  <div class="w3-display-middle" style="white-space:nowrap;">
    
    <span class="w3-center w3-padding-large w3-black w3-xlarge w3-wide w3-animate-opacity">“Let Gilsan touch <span class="w3-hide-small"> you! Be the one of our client” </span> </span>
  </div>
</div>

<!-- Container (About Section) -->
<div class="w3-content w3-container w3-padding-64" id="about">
  <h3 class="w3-center">ABOUT US</h3>
      <hr>
        <p class="w3-center"><em>Online Reservation</em></p>
        <p>Gilsan Massage and Reflexology Clinic was established in 2003 in Las Pinas City. Our hours are 7:00 AM – 10:00nn Monday through Sunday. Our Masseuse and Therapist are members of therapist Association. It provides great profession through excellence in practice, research and education as a relief to the health and wellness of the Customers. The price of the service is fixed 500 pesos only. We also help corporate social responsibility. We give free service for those who have deep sickened in their health.</p>
 </div>


<!-- Staff Section -->
<div class="bgimg-2 w3-display-container w3-opacity-min" id="staffs">
  <div class="w3-display-middle">
    <span class="w3-xxlarge w3-text-black w3-wide">STAFFS</span>
    <div class="container" style="background:white">
      <h3 class="staffs-responsive">Owner / Founder / Therapist : Susan Balang <br>
      Cleaner : Anna Magbago <br>
      Masseuse : Dixie Garcia  <br>
      Masseuse / Therapist : Analiza Arboleda <br>
      Therapist : Rina Adique <br>
      Receptionist : Mary Mendoza <br>
      </h3>
      
    </div>
   </div>

    
 </div>




 <!-- Container (Contact Section) -->
<div class="w3-content w3-container w3-padding-64" id="contactus">
  <h3 class="w3-center">CONTACT US</h3>
  <p class="w3-center"><em>Reservation as guest or Feedback </em></p>

  <div class="w3-row w3-padding-32 w3-section">
    
    <div class="w5-col m8 w3-panel">
      <div class="w3-large w3-margin-bottom">
        <i class="fa fa-map-marker fa-fw w3-hover-text-black w3-xlarge w3-margin-right"></i> 42 Schilling Street Camella 3A and B Pamplona III Las Pinas City<br>
        <i class="fa fa-phone fa-fw w3-hover-text-black w3-xlarge w3-margin-right"></i>Tel Phone: 8090960<br>
        <i class="fa fa-mobile-phone fa-fw w3-hover-text-black w3-xlarge w3-margin-right"></i>Celphone: 09214717505 / 09998073463<br>
        <i class="fa fa-envelope fa-fw w3-hover-text-black w3-xlarge w3-margin-right"></i> Email: gilsanmrc@gmail.com<br>
      </div>
      <p>Reservation as guest <i class="fa fa-user"></i>, or leave us a feedback:</p>

       <?php 
            if(!empty($success))
                {

                ?><h5 class="sucess"><?php  echo $success;
                } 
                else if(!empty($datetimelapse))
                {
                ?><h5 class="error"><?php  echo $datetimelapse;
                }
                else if(!empty($no_service))
                {
                ?><h5 class="error"><?php  echo $no_service;
                } 

                else if(!empty($no_category))
                {
                ?><h5 class="error"><?php  echo $no_category;
                } 
                 else if(!empty($no_datetime))
                {
                ?><h5 class="error"><?php  echo $no_datetime;
                } 
                  else if(!empty($error))
                {
                ?><h5 class="error"><?php  echo $error;
                } 

            
                
                ?>
              </h5>
      <div class="w3-half">
            <label>Select Request:</label>
            <select class="w3-input w3-border" name="request" id="purpose" style="margin-top:10px">
              <option id="nopurpose">Select request</option>
              <option id="reservation">Reservation as Guest</option>
              <option id="feedback">Feedback</option>

            </select>
          </div>
         
        <!-- RESERVATION -->
        <div  class="reservation"> 
      <form action=""  method="POST">
        <input type="hidden" name="mode" value="request_guess"></input>

        <div class="w3-row-padding" style="margin:0 -16px 8px -16px">
          <div class="w3-half ">
            <label>Full Name:</label>
            <input class="w3-input w3-border" type="text" placeholder="Enter Full Name" required name="fullname_guess">
          </div>
          <div class="w3-half ">
            <label>Email:</label>
            <input class="w3-input w3-border" type="text" placeholder="Enter Email" required name="email">
          </div>
           <div class="w3-half ">
            <label>Contact No:</label>
          <input class="w3-input w3-border" type="number" placeholder="Enter Contact Number"  name="contactno">
         </div>
         
          <div class="w3-half ">
            <label>Address:</label>
          <input class="w3-input w3-border" type="text" placeholder="Enter Address"  name="address">
         </div>
          <div class="form-group ">
              <label for="dtp_input1" class="col-md-2 control-label" style="position:static">DateTime Reservation</label>
                          <div class="input-group date form_datetime col-md-5" data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input1">
                              <input class="form-control" size="16" type="text" value="" name="datetime" readonly>
                              <span class="input-group-addon"><i class="fa fa-remove"></i></span></span>
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span></span>
                          </div>
                  <input type="hidden" id="dtp_input1" value="" /><br/>
            </div>

        <div class="w3-half ">
            <label>Select Type of Reservation:</label>
            <select class="w3-input w3-border" name="type_of_service" style="margin-top:10px">
              <option value="none">Select Type</option>
              <option >Massage</option>
              <option >Reflexology</option>

            </select>
          </div>
        <div class="w3-half ">
            <label>Select Category of Reservation:</label>
            <select class="w3-input w3-border" name="category_of_service" style="margin-top:10px">
              <option value="none">Select Type</option>
              <option>In Home</option>
              <option>In Clinic</option>

            </select>
          </div>

        </div>
        <div class="g-recaptcha" data-sitekey="6LfdnjoUAAAAAIYx3XMJ5kflAFFWB56jlSDDwCSu" ></div>

          <input class="w3-button w3-black w3-right w3-section " onClick="return val()"  type="submit" name="submit" value="SEND">
          
        </input>

      </form>
    </div>
      <!-- FEEDBACK -->
      <div class="feedback"> 
      <form action="" method="POST">
        <input type="hidden" name="mode" value="feedback_guess"></input>

        <div class="w3-row-padding" style="margin:0 -16px 8px -16px">
          <div class="w3-half ">
            <label>Full Name:</label>
            <input class="w3-input w3-border" type="text" placeholder="Enter Full Name" required name="fullname_guess">
          </div>
          <div class="w3-half ">
            <label>Email:</label>
            <input class="w3-input w3-border" type="email" placeholder="Enter Email" required name="email">
          </div>
         
         
        </div>
        <div>
           <div style="margin-bottom: 13px;" class="w3-half ">
            <label>Rate Service:</label>
            <select class="w3-input w3-border" name="rate_of_service" style="margin-top:10px">
              <option value="none">Select Type</option>
              <option >5 Star</option>
              <option >4 Star</option>
              <option >3 Star</option>
              <option >2 Star</option>
              <option >1 Star</option>

            </select>
          </div>

      
        </div>

          <input class="w3-input w3-border " type="text" placeholder="Enter Message"  name="message">

       

           <input class="w3-button w3-black w3-right w3-section " type="submit" name="submit" value="SEND">
          
        </input>

      </form>
    </div>
    </div>
  </div>
</div>


 <?php include 'templates/footer.php' ?>
</body>
</html>
